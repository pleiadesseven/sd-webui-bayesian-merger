# sd-webui-bayesian-merger

[![](https://dcbadge.vercel.app/api/server/EZJuBfNVHh)](https://discord.gg/EZJuBfNVHh)

## NEWS

- 2023/06/14 lots of new things, I lost track...join the discord server for discussion and future updates
- 2023/05/30 introducing [`meh`](https://github.com/s1dlx/meh) engine
- 2023/05/17 add `scorer_device`; remove `aes` and `cafe_*` scorers; add `score_weight` to payload `.yaml`
- 2023/05/16 `latin-hypercube-sampling` for `bayes` optimiser
- 2023/05/15 `adaptive-tpe` optimiser
- 2023/05/03 `tensor_sum` merging method
- 2023/04/25 `weighted_subtraction` merging method
- 2023/04/22 `manual` scoring method
- 2023/04/18 `group` parameters
- 2023/04/17 `freeze` parameters or set custom optimisation `ranges`

## What is this?

An opinionated take on stable-diffusion models-merging automatic-optimisation.

The main idea is to treat models-merging procedure as a black-box model with 26 parameters: one for each block plus `base_alpha`.
We can then try to apply black-box optimisation techniques, in particular we focus on [Bayesian optimisation](https://en.wikipedia.org/wiki/Bayesian_optimization) with a [Gaussian Process](https://en.wikipedia.org/wiki/Gaussian_process) emulator.
Read more [here](https://github.com/fmfn/BayesianOptimization), [here](http://gaussianprocess.org) and [here](https://optimization.cbe.cornell.edu/index.php?title=Bayesian_optimization).

The optimisation process is split in two phases:
1. __exploration__: here we sample (at random for now, with some heuristic in the future) the 26-parameter hyperspace, our block-weights. The number of samples is set by the
`--init_points` argument. We use each set of weights to merge the two models we use the merged model to generate `batch_size * number of payloads` images which are then scored.
2. __exploitation__: based on the exploratory phase, the optimiser makes an idea of where (i.e. which set of weights) the optimal merge is.
This information is used to sample more set of weights `--n_iters` number of times. This time we don't sample all of them in one go. Instead, we sample once, merge the models,
generate and score the images and update the optimiser knowledge about the merging space. This way the optimiser can adapt the strategy step-by-step.

At the end of the exploitation phase, the set of weights scoring the highest score are deemed to be the optimal ones.

## OK, How Do I Use It In Practice?

Head to the [wiki](https://github.com/s1dlx/sd-webui-bayesian-merger/wiki/Home) for all the instructions to get you started.

## With the help of

- [WhiteWipe/sd-webui-bayesian-merger](https://github.com/WhiteWipe/sd-webui-bayesian-merger)
- sd-webui-bayesian-merger f76686
- [fcski/otokonoko](https://huggingface.co/fcski/otokonoko)
- [sdweb-merge-block-weighted-gui](https://github.com/bbc-mc/sdweb-merge-block-weighted-gui)
- [sd-webui-supermerger](https://github.com/hako-mikan/sd-webui-supermerger)
- [sdweb-auto-MBW](https://github.com/Xerxemi/sdweb-auto-MBW)
- [SD-Chad](https://github.com/grexzen/SD-Chad.git)


[![Discord Server](https://dcbadge.vercel.app/api/server/EZJuBfNVHh)](https://discord.gg/EZJuBfNVHh)

## NEWS

- 2024/01/12 Vit-H-14 と Vit-L-14 を使った自家製モデルに対応
- 2024/01/06 WhiteWipe/sd-webui-bayesian-merger と f76686 をマージ
- 2023/06/14 多くの新しいものがあり、追いつけませんでした...ディスカッションと将来のアップデートのためにディスコードサーバーに参加してください
- 2023/05/30 [`meh`](https://github.com/s1dlx/meh) エンジンを導入
- 2023/05/17 `scorer_device` を追加; `aes` と `cafe_*` のスコアラーを削除; ペイロード `.yaml` に `score_weight` を追加
- 2023/05/16 `bayes` 最適化のための `latin-hypercube-sampling`
- 2023/05/15 `adaptive-tpe` 最適化
- 2023/05/03 `tensor_sum` マージ方法
- 2023/04/25 `weighted_subtraction` マージ方法
- 2023/04/22 `manual` スコアリング方法
- 2023/04/18 `group` パラメータ
- 2023/04/17 パラメータを `freeze` するか、カスタムの最適化 `ranges` を設定

## これは何ですか？

安定拡散モデルのマージ自動最適化に対する主観的なアプローチです。

主なアイデアは、ブロックごとに1つと `base_alpha` の26個のパラメータを持つブラックボックスモデルとしてマージ手順を扱うことです。
その後、ブラックボックス最適化技術を適用し、特に [ベイズ最適化](https://en.wikipedia.org/wiki/Bayesian_optimization) と [ガウス過程](https://en.wikipedia.org/wiki/Gaussian_process) エミュレータに焦点を当てています。
詳細は[こちら](https://github.com/fmfn/BayesianOptimization)、[こちら](http://gaussianprocess.org)、および[こちら](https://optimization.cbe.cornell.edu/index.php?title=Bayesian_optimization)をご覧ください。

最適化プロセスは2つのフェーズに分かれています：

1.__exploration__：ここでは、26個のパラメータハイパースペース、つまりブロックの重みを（現時点ではランダムに、将来的にはいくつかのヒューリスティックを使用して）サンプリングします。サンプルの数は `--init_points` 引数で設定されます。各重みセットを使用して2つのモデルをマージし、マージされたモデルを生成します。その後、`batch_size * ペイロードの数` の画像をスコアリングします。

2. __exploitation__：探索フェーズに基づいて、最適化プロセスは最適なマージの場所（つまり、どの重みセット）を把握します。この情報を使用して、`--n_iters` 回数だけさらに重みセットをサンプリングします。今回は一度にすべてをサンプリングしません。代わりに、一度サンプリングしてモデルをマージし、画像を生成してスコアリングし、最適化プロセスがマージ空間についての知識を更新します。これにより、最適化プロセスは段階的に戦略を適応させることができます。

活用フェーズの最後に、最も高いスコアを持つ重みセットが最適なものと見なされます。

## では、実際にどのように使用しますか？

始めるためのおおまかなの手順については、`config_sample.yaml` を参照してください。
