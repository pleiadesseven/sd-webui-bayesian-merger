import torch
from PIL import Image
import pytest

from sd_webui_bayesian_merger.models.RewardFunction import RewardFunction


@pytest.fixture
def reward_function():
    return RewardFunction(input_size=100, pathname="model.pth", device="cpu")


def test_load_model(reward_function):
    state_dict = reward_function.load_model()
    assert isinstance(state_dict, dict)


def test_load_clip(reward_function):
    reward_function.load_clip()
    assert reward_function.clip_model is not None
    assert reward_function.clip_preprocess is not None
    assert reward_function.tokenizer is not None
    assert reward_function.preprocess_size is not None


def test_get_image_preprocess(reward_function):
    preprocess_size = 224
    image_preprocess = reward_function.get_image_preprocess(preprocess_size)
    assert image_preprocess is not None


def test_get_image_features(reward_function):
    image_path = "image.jpg"
    image = Image.open(image_path)
    image_features = reward_function.get_image_features(image)
    assert isinstance(image_features, torch.Tensor)


def test_get_prompts_features(reward_function):
    prompt = "example prompt"
    image_features = torch.randn(1, 512)
    input_emb = reward_function.get_prompts_features(prompt, image_features)
    assert isinstance(input_emb, torch.Tensor)


def test_score(reward_function):
    prompt = "example prompt"
    image_path = "image.jpg"
    image = Image.open(image_path)
    score = reward_function.score(prompt, image)
    assert isinstance(score, float)