import os
import re
import gc
import inspect
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, Tuple
from pathlib import Path
import numpy as np
import torch
import safetensors.torch
from tqdm import tqdm


import requests



from omegaconf import DictConfig
from sd_meh import merge_methods
from sd_webui_bayesian_merger.model import SDModel
from append_merge_lora import check_keys, culc_cosine

PathT = os.PathLike

MAX_TOKENS = 77

BETA_METHODS = [
    name
    for name, fn in dict(inspect.getmembers(merge_methods, inspect.isfunction)).items()
    if "beta" in inspect.getfullargspec(fn)[0]
]


NUM_INPUT_BLOCKS = 12
NUM_MID_BLOCK = 1
NUM_OUTPUT_BLOCKS = 12
NUM_TOTAL_BLOCKS = NUM_INPUT_BLOCKS + NUM_MID_BLOCK + NUM_OUTPUT_BLOCKS

KEY_POSITION_IDS = ".".join(
    [
        "cond_stage_model",
        "transformer",
        "text_model",
        "embeddings",
        "position_ids",
    ]
)
MERGE_METHODS = dict(inspect.getmembers(merge_methods, inspect.isfunction))
NUM_MODELS_NEEDED = {
    name: 3 if "c" in inspect.getfullargspec(fn)[0] else 2
    for name, fn in MERGE_METHODS.items()
}
NUM_MODELS_NEEDED = {
    "add_difference": 3,
    "weighted_sum": 2,
    "sum_twice": 3,
    "triple_sum": 3,
}

NAI_KEYS = {
    "cond_stage_model.transformer.embeddings.": "cond_stage_model.transformer.text_model.embeddings.",
    "cond_stage_model.transformer.encoder.": "cond_stage_model.transformer.text_model.encoder.",
    "cond_stage_model.transformer.final_layer_norm.": "cond_stage_model.transformer.text_model.final_layer_norm.",
}


def fix_clip(model: Dict) -> Dict:
    if KEY_POSITION_IDS in model:
        model[KEY_POSITION_IDS] = torch.tensor(
            [list(range(MAX_TOKENS))], dtype=torch.int64
        )

    return model


def fix_key(model: Dict, key: str) -> Dict:
    for nk in NAI_KEYS:
        if key.startswith(nk):
            model[key.replace(nk, NAI_KEYS[nk])] = model[key]
            del model[key]

    return model


def fix_nai_keys(model: Dict) -> Dict:
    for k in model:
        model = fix_key(model, k)

    return model


# https://github.com/j4ded/sdweb-merge-block-weighted-gui/blob/master/scripts/mbw/merge_block_weighted.py#L115
def fix_model(model: Dict) -> Dict:
    model = fix_nai_keys(model)
    return fix_clip(model)


@dataclass
class Merger:
    cfg: DictConfig
    sims: torch.Tensor = None

    def __post_init__(self):
        self.parse_models()
        self.create_model_out_name()
        self.create_best_model_out_name()
        self.check_models_key()

    def parse_models(self):
        if self.cfg.culc_mode == "cosine" :
            self.sim = torch.nn.CosineSimilarity(dim=0)
        self.model_a = Path(self.cfg.model_a)
        self.model_b = Path(self.cfg.model_b)
        self.models = {"model_a": self.model_a, "model_b": self.model_b}
        self.model_keys = ["model_a", "model_b"]
        self.model_names = []
        for _mn in self.cfg.keys():
            if _mn in self.model_keys: continue
            if _mn[:len("model_")] == "model_":
                self.model_names.append(_mn)
        self.model_real_names = [
            self.models["model_a"].stem,
            self.models["model_b"].stem,
        ]
        self.greek_letters = ["alpha"]
        seen_models = 2
        for m in self.model_names:
            if self.cfg.merge_mode != "weighted_sum":
                if seen_models == NUM_MODELS_NEEDED[self.cfg.merge_mode]:
                    break
            if m in self.cfg:
                p = Path(self.cfg[m])
            else:
                break
            if p.exists():
                self.models[m] = p
                self.model_keys.append(m)
                self.model_real_names.append(self.models[m].stem)
            else:
                break
            seen_models += 1
            self.greek_letters.append(f"alpha_{m}")
        if self.cfg.merge_mode in BETA_METHODS:
            self.greek_letters.append("beta")
        self.model_name_suffix = f"bbwm-{'-'.join(self.model_real_names)}"
        if "output_name" in self.cfg:
            if self.cfg.output_name is not None:
                self.model_name_suffix = f"bbwm-{self.cfg.output_name}"

        try:
            if self.cfg.merge_mode != "weighted_sum":
                assert len(self.model_keys) == NUM_MODELS_NEEDED[self.cfg.merge_mode]
        except AssertionError:
            print(
                "number of models defined not compatible with merge mode, aborting optimisation"
            )
            sys.exit()
    def check_models_key(self):
        print("key名照合リストの作成")
        self.keys_flags = {}
        self.global_keys = [kk for kk in self.load_sd_model(self.models["model_a"])[0].keys()]
        for k, m in self.models.items():
            if k =="model_a": continue
            else:
                te_flag, unet_flags, block_name_flags = check_keys(*self.load_sd_model(m), self.global_keys)
                self.keys_flags[k] = {"te": te_flag, "unet": unet_flags, "block": block_name_flags}
                print(f"{k} active blocks: {te_flag}, {unet_flags}")

    def create_model_out_name(self, it: int = 0) -> None:
        model_out_name = self.model_name_suffix
        model_out_name += f"-it_{it}"
        model_out_name += ".safetensors"
        self.model_out_name = model_out_name  # this is needed to switch
        self.output_file = Path(self.model_a.parent, model_out_name)

    def create_best_model_out_name(self):
        model_out_name = self.model_name_suffix
        model_out_name += "-best"
        model_out_name += f"-{self.cfg.best_precision}bit"
        model_out_name += f".{self.cfg.best_format}"
        self.best_output_file = Path(self.model_a.parent, model_out_name)

    def remove_previous_ckpt(self, current_it: int) -> None:
        if current_it > 1 and self.output_file.exists():
            self.create_model_out_name(current_it - 1)
            print(f"Removing {self.output_file}")
            self.output_file.unlink()
        self.create_model_out_name(current_it)

    def keep_best_ckpt(self) -> None:
        if self.best_output_file.exists():
            self.best_output_file.unlink()
        self.output_file.rename(self.best_output_file)

    def load_sd_model(self, model_path: PathT) -> SDModel:
        return SDModel(model_path, self.cfg.device).load_model()

    def merge_key(
        self,
        key: str,
        thetas: Dict,
        lora_flags: Dict,
        weights: Dict,
        bases: Dict,
        best: bool,
    ) -> Tuple[str, Dict]:
        if KEY_POSITION_IDS in key:
            if self.cfg.skip_position_ids == 1:
                if not best or self.cfg.best_precision == "16":
                    return (key, thetas["model_a"][key].half())
                return (
                    key,
                    thetas["model_a"][key],
                )  # Skip position_ids key to eject effect. Value of Model A used.
            elif self.cfg.skip_position_ids == 2:
                thetas["model_a"][key] = torch.tensor(
                    [list(range(MAX_TOKENS))], dtype=torch.int64
                )
                if not best or self.cfg.best_precision == "16":
                    return (key, thetas["model_a"][key].half())
                return (key, thetas["model_a"][key])

        active_key = []
        if len(lora_flags) == 1:
            for _k, _v in lora_flags.items():
                if key not in thetas[_k]: return
                active_key.append(_k)
        else:
            for _k, _v in lora_flags.items():
                if key in thetas[_k]:
                    active_key.append(_k)
            if len(active_key) == 0: return
        merge_mode = self.cfg.merge_mode
        if merge_mode != "weighted_sum":
            if len(active_key)==1:
                merge_mode == "weighted_sum"
        #for theta in thetas.values():
        #    if key not in theta:
        #        return
        current_bases = bases
        if "model.diffusion_model." in key:
            weight_index = -1

            re_inp = re.compile(r"\.input_blocks\.(\d+)\.")  # 12
            re_mid = re.compile(r"\.middle_block\.(\d+)\.")  # 1
            re_out = re.compile(r"\.output_blocks\.(\d+)\.")  # 12

            if "time_embed" in key:
                weight_index = 0  # before input blocks
            elif ".out." in key:
                weight_index = NUM_TOTAL_BLOCKS - 1  # after output blocks
            elif m := re_inp.search(key):
                weight_index = int(m.groups()[0])
            elif re_mid.search(key):
                weight_index = NUM_INPUT_BLOCKS
            elif m := re_out.search(key):
                weight_index = NUM_INPUT_BLOCKS + NUM_MID_BLOCK + int(m.groups()[0])

            if weight_index >= NUM_TOTAL_BLOCKS:
                raise ValueError(f"illegal block index {key}")

            if weight_index >= 0:
                current_bases = {k: w[weight_index] for k, w in weights.items()}


        merged = self.merge_block(current_bases, thetas, lora_flags, key, merge_mode, active_key)

        if not best or self.cfg.best_precision == "16":
            merged = merged.half()

        return (key, merged)

    def merge_culc(self, t0, t1, t2, t0_alpha=None, t1_alpha=None, t2_alpha=None, merge_mode=None, culc_mode=None, lora_flag=False):
        if t2 is None:
            merge_mode = "weighted_sum"
        if t0_alpha is None:
            t0_alpha = 1.0
        if merge_mode is None:
            merge_mode = "weighted_sum"
        if culc_mode is None:
            culc_mode = "default"

        if merge_mode == "weighted_sum":
            if culc_mode == "default" or lora_flag:
                return t0_alpha * t0 + t1_alpha * t1
            elif culc_mode == "cosine":
                simab = self.sim(t0.to(torch.float32), t1.to(torch.float32))
                dot_product = torch.dot(t0.view(-1).to(torch.float32), t1.view(-1).to(torch.float32))
                magnitude_similarity = dot_product / (torch.norm(t0.to(torch.float32)) * torch.norm(t1.to(torch.float32)))
                combined_similarity = (simab + magnitude_similarity) / 2.0
                k = (combined_similarity - self.sims.min()) / (self.sims.max() - self.sims.min())
                k = k - t1_alpha
                k = k.clip(min=.0,max=1.)
                if t0_alpha != 1.0: t0_alpha = (1 - k)
                return t0_alpha * t0 + k * t1

    def merge_block(self, current_bases: Dict, thetas: Dict, lora_flags: Dict, key: str, merge_mode: str, active_key: list) -> Dict:
        t0 = thetas["model_a"][key]
        t1_key = active_key.pop(0)
        t1 = thetas[t1_key][key]
        target_keyname = f"alpha_{t1_key}"

        #TODO: 応急処置
        if 'alpha' not in current_bases:
            print("alpha not found, set alpha=0.5 /alphaが見つからなかったので0.5に設定｡問題おこす")
            current_bases['alpha'] = 0.5

        alpha = current_bases["alpha"]
        delta = 1
        if t1_key != "model_b":
            alpha = current_bases[target_keyname]
            delta = -1

        if merge_mode == "weighted_sum":
            if lora_flags[t1_key] is None:
                #return (1-alpha) * t0 + (alpha*delta) * t1
                return self.merge_culc(t0, t1, None, (1-alpha), (alpha*delta), culc_mode=self.cfg.culc_mode)
            elif lora_flags[t1_key] == "ia3":
                #return t0 + (t1 * t0 * (alpha*delta))
                return self.merge_culc(t0, (t1*t0), None, t1_alpha=(alpha*delta), lora_flag=True)
            else:
                if t0.shape != t1.shape:
                    t1 = t1.reshape(t0.shape)
                #return t0 + (alpha*delta) * t1
                return self.merge_culc(t0, t1, None, t1_alpha=(alpha*delta), lora_flag=True)

        t2_key = active_key.pop(0)
        t2 = thetas[t2_key][key]
        if merge_mode == "add_difference":
            if lora_flags[t1_key] == "ia3":
                t1 = t1 * t0
            if lora_flags[t2_key] == "ia3":
                t2 = t2 * t0
            if t0.shape != t1.shape:
                t1 = t1.reshape(t0.shape)
            if t0.shape != t2.shape:
                t2 = t2.reshape(t0.shape)
            return t0 + alpha * (t1 - t2)

        beta = current_bases[target_keyname]
        if merge_mode == "sum_twice":
            if lora_flags[t1_key] is None and lora_flags[t2_key] is None:
                return (1 - beta) * ((1 - alpha) * t0 + alpha * t1) + beta * t2

            elif lora_flags[t1_key] is not None and lora_flags[t2_key] is None:
                if lora_flags[t1_key] == "ia3":
                    return (1 - beta) * (t0 + (alpha * t1 * t0)) + beta * t2
                if t0.shape != t1.shape:
                    t1 = t1.reshape(t0.shape)
                return (1 - beta) * (t0 + alpha * t1) + beta * t2

            elif lora_flags[t1_key] is None and lora_flags[t2_key] is not None:
                if lora_flags[t2_key] == "ia3":
                    _theta = (1 - alpha) * t0 + alpha * t1
                    return _theta + beta * t2 * _theta
                if t0.shape != t2.shape:
                    t2 = t2.reshape(t0.shape)
                return ((1 - alpha) * t0 + alpha * t1) + beta * t2

            else:
                if lora_flags[t1_key] == "ia3":
                    _theta = t0 + alpha * t1 * t0
                else:
                    if t0.shape != t1.shape:
                        t1 = t1.reshape(t0.shape)
                    _theta = t0 + alpha * t1

                if lora_flags[t2_key] == "ia3":
                    return _theta + beta * t2 * _theta

                if t0.shape != t2.shape:
                    t2 = t2.reshape(t0.shape)
                return _theta + beta * t2

        elif merge_mode == "triple_sum":
            if lora_flags[t1_key] is None and lora_flags[t2_key] is None:
                return (1 - alpha - beta) * t0 + alpha * t1 + beta * t2
            elif lora_flags[t1_key] is not None and lora_flags[t2_key] is None:
                if lora_flags[t1_key] == "ia3":
                    return (1 - beta) * t0 + alpha * t1 * t0 + beta * t2
                if t0.shape != t1.shape:
                    t1 = t1.reshape(t0.shape)
                return (1 - beta) * t0 + alpha * t1 + beta * t2
            elif lora_flags[t1_key] is None and lora_flags[t2_key] is not None:
                if lora_flags[t2_key] == "ia3":
                    _theta = (1 - alpha) * t0 + alpha * t1
                    return _theta + beta * t2 * t0
                if t0.shape != t2.shape:
                    t2 = t2.reshape(t0.shape)
                return (1 - alpha) * t0 + alpha * t1 + beta * t2
            else:
                if lora_flags[t1_key] == "ia3":
                    _theta = t0 + alpha * t1 * t0
                else:
                    if t0.shape != t1.shape:
                        t1 = t1.reshape(t0.shape)
                    _theta = t0 + alpha * t1

                if lora_flags[t2_key] == "ia3":
                    return _theta + beta * t2 * _theta

                if t0.shape != t2.shape:
                    t2 = t2.reshape(t0.shape)
                return t0 + alpha * t1 + beta * t2

    def merge(
        self,
        weights: Dict,
        bases: Dict,
        save_best: bool = False,
    ) -> None:
        # bases = {f"base_{k}": v for k, v in bases.items()}
        # option_payload = {
        #     "merge_method": self.cfg.merge_mode,
        #     **{k: str(v) for k, v in self.models.items()},
        #     **bases,
        #     **weights,
        #     "precision": self.cfg.best_precision,
        #     "device": self.cfg.device,
        #     "work_device": self.cfg.work_device,
        #     "prune": self.cfg.prune,
        #     "threads": self.cfg.threads,
        #     "destination": str(self.best_output_file) if save_best else "memory",
        #     "unload_before": True,
        #     "re_basin": self.cfg.rebasin,
        #     "re_basin_iterations": self.cfg.rebasin_iterations,
        # }

        # print("Merging models")
        # r = requests.post(
        #     url=f"{self.cfg.url}/bbwm/merge-models",
        #     json=option_payload,
        # )
        # r.raise_for_status()
        thetas = {}
        lora_flags = {}
        loadkeys = []
        if self.cfg.merge_mode == "weighted_sum":
            thetas["model_a"], _ = self.load_sd_model(self.models["model_a"])
            thetas["model_b"], lora_flags["model_b"] = self.load_sd_model(self.models["model_b"])
            theta_repeat = len(self.models) -1
            for key in self.models.keys():
                if "model_a" == key: continue
                if "model_b" == key: continue
                loadkeys.append(key)
        else:
            for key, _ in self.models.items():
                if "model_a" == key:
                    thetas["model_a"], _ = self.load_sd_model(self.models["model_a"])
                else:
                    thetas[key], lora_flags[key] = self.load_sd_model(self.models[key])
            theta_repeat = 1

        for nums in range(theta_repeat):
            if nums >= 1:
                get_key = loadkeys.pop(0)
                thetas["model_b"], lora_flags["model_b"] = self.load_sd_model(self.models[get_key])
            # cosine計算式(とりあえずweighted_sum､model_bがLoraでない時のみ)
            if self.cfg.merge_mode=="weighted_sum" and not lora_flags["model_b"]:
                print("culc: cosine")
                self.sim = torch.nn.CosineSimilarity(dim=0)
                self.sims = culc_cosine(self.sim, thetas["model_a"], thetas["model_b"])

            for key in tqdm(thetas["model_a"].keys(), desc=f"stage 1({nums+1}/{theta_repeat})"):
                if "first_stage_model" in key: continue # VAE部分はマージ処理してもいいことないので飛ばす
                if result := self.merge_key(
                    key,
                    thetas,
                    lora_flags,
                    weights,
                    bases,
                    save_best,
                ):
                    thetas["model_a"][key] = result[1]
            # メモリ解放
            if self.cfg.merge_mode=="weighted_sum" and self.cfg.culc_mode == "cosine" and not lora_flags["model_b"]:
                del self.sim, self.sims
                gc.collect()

        for key in tqdm(thetas["model_b"].keys(), desc="stage 2"):
            if "model" in key and key not in thetas["model_a"]:
                if KEY_POSITION_IDS in key:
                    if self.cfg.skip_position_ids == 1:
                        continue
                    elif self.cfg.skip_position_ids == 2:
                        thetas["model_a"][key] = torch.tensor(
                            [list(range(MAX_TOKENS))], dtype=torch.int64
                        )
                        if not save_best or self.cfg.best_precision == "16":
                            thetas["model_a"][key] = thetas["model_a"][key].half()
                        continue
                thetas["model_a"].update({key: thetas["model_b"][key]})
                if not save_best or self.cfg.best_precision == "16":
                    thetas["model_a"][key] = thetas["model_a"][key].half()

        thetas["model_a"] = fix_model(thetas["model_a"])

        if save_best:
            print(f"Saving {self.best_output_file}")
            if self.cfg.best_format == "safetensors":
                safetensors.torch.save_file(
                    thetas["model_a"],
                    self.best_output_file,
                    metadata={"format": "pt"},
                )
            else:
                torch.save({"state_dict": thetas["model_a"]}, self.best_output_file)
        else:
            print(f"Saving {self.output_file}")
            safetensors.torch.save_file(
                thetas["model_a"],
                self.output_file,
                metadata={"format": "pt", "precision": "fp16"},
            )