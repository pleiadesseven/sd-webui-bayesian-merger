from functools import partial

from hyperopt import STATUS_OK, Trials, fmin, hp, tpe, hp, fmin, tpe, STATUS_OK

from sd_webui_bayesian_merger.optimiser import Optimiser

from hyperopt import Trials, hp, fmin, tpe, STATUS_OK


class TPEOptimiser(Optimiser):
    def _target_function(self, params):
        res = self.sd_target_function(**params)
        return {
            "loss": -res,
            "status": STATUS_OK,
            "params": params,
        }

    def optimise(self) -> None:
        bounds = self.init_params()
        space = {p: hp.uniform(p, *b) for p, b in bounds.items()}
        for _, key in enumerate(self.merger.keys_flags.keys()):
            target_keyname = f"alpha_{key}"
            if key == "model_b":
                target_keyname = f"alpha"
            for i in range(len(self.merger.keys_flags[key]["unet"])):
                my_min = 0.0
                my_max = 1.0
                if f"{target_keyname}_{i}_min" in self.cfg:
                    my_min = self.cfg[f"{target_keyname}_{i}_min"]
                elif f"{target_keyname}_min" in self.cfg:
                    my_min = self.cfg[f"{target_keyname}_min"]
                elif "unet_min" in self.cfg:
                    my_min = self.cfg["unet_min"]
                elif "global_min" in self.cfg:
                    my_min = self.cfg["global_min"]

                if f"{target_keyname}_{i}_max" in self.cfg:
                    my_max = self.cfg[f"{target_keyname}_{i}_max"]
                elif f"{target_keyname}_max" in self.cfg:
                    my_max = self.cfg[f"{target_keyname}_max"]
                elif "unet_max" in self.cfg:
                    my_max = self.cfg["unet_max"]
                elif "global_max" in self.cfg:
                    my_max = self.cfg["global_max"]

                if my_min is None: my_min = 0.0
                if my_max is None: my_max = 1.0
                if self.merger.keys_flags[key]["unet"][i]:
                    space[f"block_{i}_{target_keyname}"] = hp.uniform(f"block_{i}_{target_keyname}", my_min, my_max)
            if self.merger.keys_flags[key]["te"]:
                my_min = 0.0
                my_max = 1.0
                if f"base_{target_keyname}_min" in self.cfg:
                    my_min = self.cfg[f"base_{target_keyname}_min"]
                elif "te_min" in self.cfg:
                    my_min = self.cfg["te_min"]
                elif "global_min" in self.cfg:
                    my_min = self.cfg["global_min"]

                if f"base_{target_keyname}_max" in self.cfg:
                    my_max = self.cfg[f"base_{target_keyname}_max"]
                elif "te_max" in self.cfg:
                    my_max = self.cfg["te_max"]
                elif "global_max" in self.cfg:
                    my_max = self.cfg["global_max"]

                if my_min is None: my_min = 0.0
                if my_max is None: my_max = 1.0
                space[f"base_{target_keyname}"] = hp.uniform(f"base_{target_keyname}", my_min, my_max)
                

        self.trials = Trials()
        tpe._default_n_startup_jobs = self.cfg.init_points
        algo = partial(tpe.suggest, n_startup_jobs=self.cfg.init_points)
        fmin(
            self._target_function,
            space=space,
            algo=algo,
            trials=self.trials,
            max_evals=self.cfg.init_points + self.cfg.n_iters,
        )

        # clean up and remove the last merge
        try:
            self.cleanup()
        except FileNotFoundError:
            return

    def postprocess(self) -> None:
        print("\nRecap!")
        scores = []
        for i, res in enumerate(self.trials.losses()):
            print(f"Iteration {i} loss: \n\t{res}")
            scores.append(res)
        best = self.trials.best_trial

        best_weights, best_bases = self.bounds_initialiser.assemble_params(
            best["result"]["params"],
            self.merger.greek_letters,
            self.cfg.optimisation_guide.frozen_params
            if self.cfg.guided_optimisation
            else None,
            self.cfg.optimisation_guide.groups
            if self.cfg.guided_optimisation
            else None,
        )
        for _, key in enumerate(self.merger.keys_flags.keys()):
            target_keyname = f"alpha_{key}"
            if key == "model_b":
                target_keyname = f"alpha"
            best_weights[target_keyname] = []
            for i in range(len(self.merger.keys_flags[key]["unet"])):
                if self.merger.keys_flags[key]["unet"][i]:
                    best_weights[target_keyname].append(best["result"]["params"][f"block_{i}_{target_keyname}"])
                else:
                    best_weights[target_keyname].append(0.0)
            
            if self.merger.keys_flags[key]["te"]:
                best_bases[target_keyname] = best["result"]["params"][f"base_{target_keyname}"]
            else:
                best_bases[target_keyname] = 0.0

        self.plot_and_save(
            scores,
            best_bases,
            best_weights,
            minimise=True,
        )