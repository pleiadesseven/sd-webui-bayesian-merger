from typing import Dict, List, Tuple

from bayes_opt import BayesianOptimization, Events
from bayes_opt.domain_reduction import SequentialDomainReductionTransformer
from scipy.stats import qmc

from sd_webui_bayesian_merger.optimiser import Optimiser


class BayesOptimiser(Optimiser):
    bounds_transformer = SequentialDomainReductionTransformer()

    def optimise(self) -> None:
        # TODO: what if we want to optimise only certain blocks?
        pbounds = {}
        for _, key in enumerate(self.merger.keys_flags.keys()):
            target_keyname = f"alpha_{key}"
            if key == "model_b":
                target_keyname = f"alpha"

            for i in range(len(self.merger.keys_flags[key]["unet"])):
                my_min = 0.0
                my_max = 1.0
                if f"{target_keyname}_{i}_min" in self.cfg:
                    my_min = self.cfg[f"{target_keyname}_{i}_min"]
                elif f"{target_keyname}_min" in self.cfg:
                    my_min = self.cfg[f"{target_keyname}_min"]
                elif "unet_min" in self.cfg:
                    my_min = self.cfg["unet_min"]
                elif "global_min" in self.cfg:
                    my_min = self.cfg["global_min"]

                if f"{target_keyname}_{i}_max" in self.cfg:
                    my_max = self.cfg[f"{target_keyname}_{i}_max"]
                elif f"{target_keyname}_max" in self.cfg:
                    my_max = self.cfg[f"{target_keyname}_max"]
                elif "unet_max" in self.cfg:
                    my_max = self.cfg["unet_max"]
                elif "global_max" in self.cfg:
                    my_max = self.cfg["global_max"]

                if my_min is None: my_min = 0.0
                if my_max is None: my_max = 1.0
                if self.merger.keys_flags[key]["unet"][i]:
                    pbounds[f"block_{i}_{target_keyname}"] = (my_min, my_max)
            if self.merger.keys_flags[key]["te"]:
                my_min = 0.0
                my_max = 1.0
                if f"base_{target_keyname}_min" in self.cfg:
                    my_min = self.cfg[f"base_{target_keyname}_min"]
                elif "te_min" in self.cfg:
                    my_min = self.cfg["te_min"]
                elif "global_min" in self.cfg:
                    my_min = self.cfg["global_min"]

                if f"base_{target_keyname}_max" in self.cfg:
                    my_max = self.cfg[f"base_{target_keyname}_max"]
                elif "te_max" in self.cfg:
                    my_max = self.cfg["te_max"]
                elif "global_max" in self.cfg:
                    my_max = self.cfg["global_max"]

                if my_min is None: my_min = 0.0
                if my_max is None: my_max = 1.0
                pbounds[f"base_{target_keyname}"] = (my_min, my_max)
        
        # TODO: fork bayesian-optimisation and add LHS
        self.optimizer = BayesianOptimization(
            f=self.sd_target_function,
            pbounds=pbounds,
            random_state=1,
            bounds_transformer=self.bounds_transformer
            if self.cfg.bounds_transformer
            else None,
        )

        self.optimizer.subscribe(Events.OPTIMIZATION_STEP, self.logger)

        init_points = self.cfg.init_points
        if self.cfg.latin_hypercube_sampling:
            sampler = qmc.LatinHypercube(d=len(pbounds))
            samples = sampler.random(self.cfg.init_points)
            l_bounds = [b[0] for b in pbounds.values()]
            u_bounds = [b[1] for b in pbounds.values()]
            scaled_samples = qmc.scale(samples, l_bounds, u_bounds)

            for sample in scaled_samples.tolist():
                params = dict(zip(pbounds, sample))
                self.optimizer.probe(params=params, lazy=True)

            init_points = 0

        self.optimizer.maximize(
            init_points=self.cfg.init_points,
            n_iter=self.cfg.n_iters,
        )

        # clean up and remove the last merge
        try:
            self.cleanup()
        except FileNotFoundError:
            return

    def postprocess(self) -> None:
        print("\nRecap!")
        for i, res in enumerate(self.optimizer.res):
            print(f"Iteration {i}: \n\t{res}")

        scores = parse_scores(self.optimizer.res)
        best_weights, best_bases = self.bounds_initialiser.assemble_params(
            self.optimizer.max["params"],
            self.merger.greek_letters,
            self.cfg.optimisation_guide.frozen_params
            if self.cfg.guided_optimisation
            else None,
            self.cfg.optimisation_guide.groups
            if self.cfg.guided_optimisation
            else None,
        )

        self.plot_and_save(
            scores,
            best_bases,
            best_weights,
            minimise=False,
        )

    def parse_params(self, params: Dict) -> Tuple[Dict, Dict]:
        bases = {}
        weights = {}
        for _, key in enumerate(self.merger.keys_flags.keys()):
            target_keyname = f"alpha_{key}"
            if key == "model_b":
                target_keyname = f"alpha"
            weights[target_keyname] = []
            for i in range(len(self.merger.keys_flags[key]["unet"])):
                if self.merger.keys_flags[key]["unet"][i]:
                    weights[target_keyname].append(params[f"block_{i}_{target_keyname}"])
                else:
                    weights[target_keyname].append(0.0)

            if self.merger.keys_flags[key]["te"]:
                bases[target_keyname] = params[f"base_{target_keyname}"]
            else:
                bases[target_keyname] = 0.0
        return bases, weights


def parse_scores(iterations: List[Dict]) -> List[float]:
    return [r["target"] for r in iterations]
