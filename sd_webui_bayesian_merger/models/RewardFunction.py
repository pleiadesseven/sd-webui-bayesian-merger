from pathlib import Path
import torch
import torch.nn as nn
import PIL.Image as Image
import safetensors
import clip
from torchvision.transforms import Compose, Resize, CenterCrop, ToTensor, Normalize
try:
    from torchvision.transforms import InterpolationMode
    BICUBIC = InterpolationMode.BICUBIC
except ImportError:
    BICUBIC = Image.BICUBIC

class MLP(nn.Module):
    def __init__(self, input_size, xcol='emb', ycol='avg_rating'):
        super().__init__()
        self.input_size = input_size
        self.xcol = xcol
        self.ycol = ycol
        self.input_size = input_size
        self.hidden_size = max(self.input_size//2, 1024)
        self.layers = torch.nn.Sequential(
            torch.nn.Linear(self.input_size, self.hidden_size),
            torch.nn.ReLU(),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(self.hidden_size, 1)
        )
    def forward(self, x):
        return self.layers(x)


class RewardFunction(nn.Module):
    def __init__(self, input_size, pathname, device, max_token):
        super().__init__()
        self.device = device
        self.max_token = max_token
        self.model_name = Path(pathname).name
        self.pathname= pathname
        self.mlp = MLP(input_size)
        state_dict = self.load_model()
        self.mlp.load_state_dict(state_dict, strict=False)
        self.mlp = self.mlp.to(self.device)
        self.mlp.eval()
        self.load_clip()

    def _convert_image_to_rgb(self, image):
        return image.convert("RGB")

    def _transform(self, n_px):
        return Compose([
            Resize(n_px, interpolation=BICUBIC),
            CenterCrop(n_px),
            self._convert_image_to_rgb,
            ToTensor(),
            Normalize((0.48145466, 0.4578275, 0.40821073), (0.26862954, 0.26130258, 0.27577711)),
        ])

    def get_image_preprocess(self, preprocess_size, scale=1):
            """
            Returns the preprocessed image based on the given preprocess size and scale.

            Args:
                preprocess_size (int): The size of the image to be preprocessed.
                scale (float, optional): The scaling factor for the image. Defaults to 1.

            Returns:
                Preprocessed image.
            """
            return self._transform(preprocess_size*scale)

    def load_model(self) -> None:
        if Path(self.pathname).suffix == ".safetensors":
            state_dict = safetensors.torch.load_file(self.pathname)
        else:
            state_dict = torch.load(self.pathname, map_location=self.device)
        return state_dict

    def load_clip(self) -> None:
        """
        Loads the CLIP model and sets up necessary attributes based on the file path.

        """
        self.clip_model, self.clip_preprocess = clip.load("ViT-L/14",device=self.device)
        self.tokenizer = clip.tokenize
        self.preprocess_size = self.clip_model.visual.input_resolution
        if any(substring in self.model_name for substring in ["rf2", "rf3"]):
            self.image_preprocess = self.get_image_preprocess(self.preprocess_size, 2)
        self.clip_model.eval()

    def get_prompts_features(self, prompt: str, image_features) -> torch.Tensor:
        """Get the features of prompts.

        Args:
            prompt (str): The prompt text.
            image_features (_type_): The features of the image.

        Returns:
            torch.Tensor: The input embeddings.
        """
        token_chunk = 77
        token_split_num = self.max_token // token_chunk

        txt_id = self.tokenizer(prompt, context_length=self.max_token, truncate=True).to(self.device)

        txt_features_list = []
        for i in range(token_split_num):
            txt_features = self.clip_model.encode_text(txt_id[:,i*token_chunk:(i+1)*token_chunk])
            txt_features_list.append(txt_features)
        if token_split_num>1:
            txt_features = torch.concat(txt_features_list, dim=1)

        input_emb = torch.concat([image_features, txt_features], dim=1)
        input_emb = input_emb.cpu().detach().numpy()
        return input_emb

    def get_image_features(self, image: Image.Image) -> torch.Tensor:
        """
        Extracts image features using the specified algorithm.

        Args:
            image (PIL.Image.Image): The input image.

        Returns:
            torch.Tensor: The extracted image features.
        """
        if any(substring in self.model_name for substring in ["rf", "rf1", "rf2", "rf3"]):
            _image = self.clip_preprocess(image).unsqueeze(0).to(self.device)
            with torch.no_grad():
                image_features = self.clip_model.encode_image(_image)
            if any(substring in self.model_name for substring in ["rf2", "rf3"]):
                _image = self.image_preprocess(image).unsqueeze(0).to(self.device)
                for i in range(2):
                    for j in range(2):
                        with torch.no_grad():
                            image_features = torch.concat([image_features, self.clip_model.encode_image(_image[:,:,i*self.preprocess_size:(i+1)*self.preprocess_size,j*self.preprocess_size:(j+1)*self.preprocess_size])], dim=1)
            return image_features

    def score(self, prompt: str, image: Image.Image) -> float:
        image_features = self.get_image_features(image)
        image_features = self.get_prompts_features(prompt, image_features)

        score = self.mlp(torch.from_numpy(image_features).to(self.device).float())

        #TODO:しきい値がある場合の処理 後回し
        # if self.cfg.rf_score_threshold is not None:
        #     if score > self.rf_score_threshold:
        #         score = torch.atan((score-self.rf_score_threshold)*self.attenuation) + self.rf_score_threshold

        return score.item()