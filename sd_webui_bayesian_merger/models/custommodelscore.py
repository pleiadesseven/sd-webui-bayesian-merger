#import webdataset as wds
from PIL import Image
#import io
#import matplotlib.pyplot as plt
#import os
#import json

from warnings import filterwarnings


# os.environ["CUDA_VISIBLE_DEVICES"] = "0"    # choose GPU if you are on a multi GPU server
import numpy as np
import torch
import pytorch_lightning as pl
import torch.nn as nn
from torchvision import datasets, transforms
#import tqdm
import torch.nn.functional as F

from os.path import join
#from datasets import load_dataset
import pandas as pd
from torch.utils.data import Dataset, DataLoader
import json
import glob
import os
import shutil
import tqdm
import argparse

import clip
import open_clip


from PIL import Image, ImageFile

class MLP(nn.Module):
    def __init__(self, input_size):
        super().__init__()
        self.input_size = input_size
        self.layers = nn.Sequential(
            nn.Linear(self.input_size, 1024),
            # nn.ReLU(),
            nn.Dropout(0.2),
            nn.Linear(1024, 128),
            # nn.ReLU(),
            nn.Dropout(0.2),
            nn.Linear(128, 64),
            # nn.ReLU(),
            nn.Dropout(0.1),

            nn.Linear(64, 16),
            # nn.ReLU(),

            nn.Linear(16, 1)
        )

    def forward(self, x):
        return self.layers(x)


class Custoum(nn.Module):
    def __init__(self, pathname, clip_path, clip_model_base, device):
        super().__init__()
        self.device = device
        self.clip_path = clip_path
        self.clip_model_base = clip_model_base
        self.clip_load_or_open()
        embedding_size = self.clip_model.visual.output_dim # 埋め込みサイズを動的に取得
        self.mlp = MLP(embedding_size) # CLIP ViT L 14 の場合、CLIP 埋め込みサイズは 768 H 14 は1024
        state_dict = torch.load(pathname, map_location='cpu')
        self.mlp.load_state_dict(state_dict, strict=False)
        self.mlp = self.mlp.to(self.device)
        self.mlp.eval()

        self.clip_model.float()
        self.clip_model.logit_scale.requires_grad_(False)


    def clip_load_or_open(self):
        try:
            self.clip_model, self.preprocess = clip.load(
                name=self.clip_model_base, device=self.device, jit=False)
            print(f"CLIPライブラリを使用してロードされたCLIPモデル: {self.clip_model_base}")
        except:
            try:
                self.clip_model, self.preprocess, _ = open_clip.create_model_and_transforms(
                    self.clip_model_base, device=self.device)
                print(f"open_clipライブラリを使用してロードされたCLIPモデル: {self.clip_model_base}")
            except Exception as ex:
                raise RuntimeError(
                f"Clip と open_clip ライブラリの両方を使用してモデルをロードできませんでした. Error: {ex}") from ex



    def training_step(self, batch, batch_idx):
            x = batch[self.xcol]
            y = batch[self.ycol].reshape(-1, 1)
            x_hat = self.layers(x)
            loss = F.mse_loss(x_hat, y)
            return loss

    def validation_step(self, batch, batch_idx):
        x = batch[self.xcol]
        y = batch[self.ycol].reshape(-1, 1)
        x_hat = self.layers(x)
        loss = F.mse_loss(x_hat, y)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def normalized(a, axis=-1, order=2):
        import numpy as np  # pylint: disable=import-outside-toplevel

        l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
        l2[l2 == 0] = 1
        return a / np.expand_dims(l2, axis)

    def score(self, prompt, image):
        # 画像処理
        if isinstance(image, Image.Image):
            pil_image = image
        elif isinstance(image, str):
            if os.path.isfile(image):
                pil_image = Image.open(image)
        image = self.preprocess(pil_image).unsqueeze(0).to(self.device)
        image_features = F.normalize(self.clip_model.encode_image(image)).float()

        # スコア計算
        with torch.no_grad():
            rewards = self.mlp(image_features)

        return rewards.detach().cpu().numpy().item()
    
        # self.clip_model.logit_scale.requires_grad_(False)
        #     device = "cuda" if torch.cuda.is_available() else "cpu"
        #     model2, preprocess = clip.load("ViT-L/14", device=device)  #RN50x64
        #     output_str_tr = ""
        #     output_str_img = ""
        #     output_str_score = ""
        #     i = 0
        #     for img_path in tqdm.tqdm(img_path_list):
        #         pil_image = Image.open(img_path)

        #         image = preprocess(pil_image).unsqueeze(0).to(device)



        #         with torch.no_grad():
        #             image_features = model2.encode_image(image)

        #         im_emb_arr = normalized(image_features.cpu().detach().numpy() )

        #         prediction = model(torch.from_numpy(im_emb_arr).to(device).type(torch.cuda.FloatTensor))

        #         #print( f"img : {img_path}")
        #         #print( f"Aesthetic score predicted by the model: {prediction}")
        #         score = prediction.data.cpu()[0][0]

    def inference_rank(self, prompt, generations_list):

        img_set = []
        for generations in generations_list:
            # image encode
            img_path = generations
            pil_image = Image.open(img_path)
            image = self.preprocess(pil_image).unsqueeze(0).to(self.device)
            image_features = F.normalize(self.clip_model.encode_image(image))
            img_set.append(image_features)

        img_features = torch.cat(img_set, 0).float()  # [image_num, feature_dim]
        rewards = self.mlp(img_features)
        rewards = torch.squeeze(rewards)
        _, rank = torch.sort(rewards, dim=0, descending=True)
        _, indices = torch.sort(rank, dim=0)
        indices = indices + 1

        return indices.detach().cpu().numpy().tolist(), rewards.detach().cpu().numpy().tolist()

